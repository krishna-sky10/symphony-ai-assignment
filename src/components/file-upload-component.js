import React from "react";
import { enUS } from "../translations";

function FileUpload(props) {
  return (
    <form
      className="file-upload"
      encType="multipart/form-data"
      method="post"
      name="fileinfo"
      action="/upload"
    >
      <input
        type="file"
        className="select-file btn btn-md btn-secondary"
        name="selectedFile"
        onChange={props.handleFileChange}
      />
      <button
        type="submit"
        className="submit btn btn-md btn-primary"
        disabled={props.disabled}
        onClick={props.handleUpload}
      >
        {enUS["upload-file"]}
      </button>
    </form>
  );
}

export default FileUpload;
