const fs = require("fs");
const readline = require("readline");

async function processFile(filePath) {
  const fileStream = fs.createReadStream(filePath);
  const lines = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity,
  });
  let outputData = [];

  for await (const line of lines) {
    const splitArray = line.split("|");
    outputData.push(splitArray);
  }

  return outputData;
}

module.exports = {
  processFile,
};
