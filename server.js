const {
  NODEJS_EXPRESS_SERVER_PORT,
  UPLOAD_DIR,
  BUILD_PATH,
  FILE_NAME,
} = require("./src/constants/app-constants");
const port = process.env.PORT || NODEJS_EXPRESS_SERVER_PORT;
const express = require("express");
const bodyParser = require("body-parser");
const multer = require("multer");
const path = require("path");
const cors = require("cors");
const processFile = require("./src/utils/server-utils").processFile;

const storage = multer.diskStorage({
  destination: (request, file, cb) => {
    cb(null, path.join(__dirname + UPLOAD_DIR));
  },
  filename: (request, file, cb) => {
    cb(null, file.originalname);
  },
});
const app = express();
const buildPath = path.join(__dirname, BUILD_PATH);

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(buildPath));
app.get("/*", function (req, res) {
  res.sendFile(path.join(buildPath, "index.html"));
});

const upload = multer({ storage });
const uploadFileMiddleware = upload.single(FILE_NAME);

app.post("/upload", uploadFileMiddleware, (request, response) => {
  processFile(request.file.path).then((resolve, reject) => {
    response.json(resolve);
  });
});

app.listen(port, () => console.log(`Server listening on port ${port}`));
