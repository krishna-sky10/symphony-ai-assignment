This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### Initial setup
### `npm run install`
### `npm run build`

### Production Mode

### `node server.js`

Runs the app in the production mode using the **build** folder.  
Open [http://localhost:3001](http://localhost:3001) to view it in the browser.

### Development Mode

### `npm start`

Runs the react app in the development mode.  
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.  
You will also see any lint errors in the console.  
**You will not be able to access backend NodeJS - Express server.** Follow below steps for that.

### `node server.js`

Runs the NodeJS - Express server in development mode.  
Which will run on [http://localhost:3001](http://localhost:3001) in local system.  
Now you can browse and upload files then view its content in table.

### How To Use

After starting the required servers. Follow these steps:

1. Browse a file via the file input control provided.
2. Upload the file by clicking on upload button. It will upload the selected file in local file storage of the server system.
3. Filter table data using the delimiter and lines input controls in real time.
4. Restart from step 1 to keep uploading new files.

#### **\*\*The delimiter filter is incomplete because the problem statement for delimiter filter was ambiguous and unclear.**
