import React from "react";

function SimpleTable(props) {
  const { tableData, lines } = props;
  const newTableData = tableData.slice(0, lines);

  return (
    <div className="simple-table">
      <table className="table table-striped table-bordered">
        <thead></thead>
        <tbody>
          {newTableData.map((row, index) => {
            return (
              <tr key={`tr-${index}`}>
                <td>{row[0]}</td>
                <td>{row[1]}</td>
                <td>{row[2]}</td>
                <td>{row[3]}</td>
                <td>{row[4]}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default SimpleTable;
