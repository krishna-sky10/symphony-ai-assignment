import React from "react";
import { enUS } from "../translations";

function FilterTable(props) {
  return (
    <div className="filter-table">
      <div className="input-group input-group-sm mb-3 filter-delimiter">
        <div className="input-group-prepend">
          <span className="input-group-text">{enUS["delimiter"]}</span>
          <input
            className="form-control delimiter"
            autoFocus
            value={props.delimiter}
            onChange={props.handleDelimiterChange}
          />
        </div>
      </div>
      <div className="input-group input-group-sm mb-3 filter-lines">
        <div className="input-group-prepend">
          <span className="input-group-text">{enUS["lines"]}</span>
          <input
            className="form-control lines"
            value={props.lines}
            onChange={props.handleLinesChange}
          />
        </div>
      </div>
    </div>
  );
}

export default FilterTable;
