import React, { useState } from "react";
import FileUpload from "./file-upload-component";
import FilterTable from "./filter-table-component";
import SimpleTable from "./simple-table-component";
import axios from "axios";
import {
  HTTP_SERVER_BASE_URL,
  UPLOAD_URL_PATH,
  NODEJS_EXPRESS_SERVER_PORT,
  FILE_NAME,
} from "../constants/app-constants";

function DynamicTable(props) {
  const [delimiter, setDelimiter] = useState("");
  const [lines, setLines] = useState(2);
  const [tableData, setTableData] = useState([]);
  const [isUploading, setIsUploading] = useState(false);
  const [selectedFile, setSelectedFile] = useState("");

  const _handleFileChange = (event) => {
    console.log(event, event.target.files[0], event.target.name);
    setSelectedFile(event.target.files[0]);
  };

  const _handleUpload = (event) => {
    let formData = new FormData();

    event.preventDefault();
    formData.append(FILE_NAME, selectedFile);

    setIsUploading(true);

    const serverUploadURL = `${HTTP_SERVER_BASE_URL}:${NODEJS_EXPRESS_SERVER_PORT}${UPLOAD_URL_PATH}`;
    axios.post(serverUploadURL, formData).then((result) => {
      setIsUploading(false);
      setTableData(result.data);
    });
  };

  const _handleDelimiterChange = (event) => {
    setDelimiter(event.target.value);
  };

  const _handleLinesChange = (event) => {
    setLines(event.target.value);
  };

  return (
    <div className="text-body bg-light bg-gradient-primary dynamic-table">
      <FileUpload
        disabled={isUploading}
        handleFileChange={_handleFileChange}
        handleUpload={_handleUpload}
      />
      <FilterTable
        delimiter={delimiter}
        lines={lines}
        handleDelimiterChange={_handleDelimiterChange}
        handleLinesChange={_handleLinesChange}
      />
      <SimpleTable tableData={tableData} lines={lines} />
    </div>
  );
}

export default DynamicTable;
