import React from "react";
import { DynamicTable } from "./components";
import "./app.css";

function App() {
  return (
    <div className="container app">
      <DynamicTable />
    </div>
  );
}

export default App;
