const HTTP_SERVER_BASE_URL = "http://localhost";
const FILE_NAME = "selectedFile";
const UPLOAD_DIR = "/uploads";
const UPLOAD_URL_PATH = "/upload";
const BUILD_PATH = "build";
const REACT_DEV_SERVER_PORT = 3000;
const NODEJS_EXPRESS_SERVER_PORT = 3001;

module.exports = {
  HTTP_SERVER_BASE_URL,
  FILE_NAME,
  UPLOAD_DIR,
  UPLOAD_URL_PATH,
  REACT_DEV_SERVER_PORT,
  NODEJS_EXPRESS_SERVER_PORT,
  BUILD_PATH,
};
